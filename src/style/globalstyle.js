import { createGlobalStyle } from 'styled-components'

export default createGlobalStyle`

* {
    margin: 0;
    padding: 0;
    box-sizing: border-box
}

body{
    background: ${props => props.theme.colors.background};
    font-size: 15px;
    color: ${props => props.theme.colors.text};
    font-family: Arial, Helvetica, sans-serif; 
}

h1 {
    font-size: 35px;
    display: flex;
    justify-content: center;
    margin: auto;
    color: ${props => props.theme.colors.text};
}

`
