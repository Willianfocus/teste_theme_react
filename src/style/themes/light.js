
export default {
    title: 'light',

    colors: {
        primary: '#ff6633',
        secundary: '#AC416F',

        background: '#f5f5f5',
        text: '#333',
    }
}