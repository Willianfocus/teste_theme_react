
export default {
    title: 'dark',

    colors: {
        primary: '#333333',
        secundary: '#AC416F',

        background: '#404040',
        text: '#fff',
    }
}