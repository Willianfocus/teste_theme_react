import React from 'react'
import { ThemeProvider } from 'styled-components'
import light from './style/themes/light'
import dark from './style/themes/dark'
import focus from './style/themes/focus'

import Header from './components/header'
import Footer from './components/footer'

import Globalstyles from './style/globalstyle'



function App() {
  return (
    <ThemeProvider theme={}>
      <div className="App">
        <Header />

        <h1>Teste de temas</h1>

        <Globalstyles />
      </div>
    </ThemeProvider>
  );
}

export default App;
